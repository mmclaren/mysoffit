package edu.bristol.mysoffit;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/test")
public class Api {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public @ResponseBody
    String getTest(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getRemoteUser();
    }

}
