package edu.bristol.mysoffit;

import org.apereo.portal.soffit.renderer.SoffitApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SoffitApplication
@SpringBootApplication
public class MysoffitApplication {

	public static void main(String[] args) {
		SpringApplication.run(MysoffitApplication.class, args);
	}

}
