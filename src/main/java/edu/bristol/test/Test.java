package edu.bristol.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apereo.portal.soffit.model.v1_0.Bearer;
import org.apereo.portal.soffit.service.AbstractJwtService;
import org.apereo.portal.soffit.service.BearerService;
import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

//@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class Test implements CommandLineRunner {

    private static BearerService bearerService;

    public static void main(String[] args) {
        new SpringApplicationBuilder(Test.class)
                .web(WebApplicationType.NONE).run(args);
    }

    @Bean
    public BearerService bearerService() {
        bearerService = new BearerService();
        return bearerService;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello");
        String username = "testuser";
        
        Map<String, List<String>> attributes = new HashMap<>();
        List<String> groups = new ArrayList<>();
        Date expires = null;
        Bearer bearer = bearerService.createBearer(username, attributes, groups, expires);        
        String encryptedToken = bearer.getEncryptedToken();
        System.out.println(encryptedToken);  
       
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(AbstractJwtService.DEFAULT_ENCRYPTION_PASSWORD);

        // Decryption
        final String jwt = textEncryptor.decrypt(encryptedToken);
        System.out.println(jwt);
        
        System.out.println();
        System.out.println();

        System.out.println(String.format(
                "curl -H \"Accept: application/json\" "
                        + "-H \"Authorization: Bearer %s\" "
                        + "http://localhost:8090/api/test/", jwt));
        
    }

}
